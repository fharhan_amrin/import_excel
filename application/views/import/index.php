    <div class="row">
        <div class="col-lg-12">
            <h1>Lawyers <small>Overview</small></h1>
            <ol class="breadcrumb">
                <li class="active"><i class="fa fa-list"></i> Import Members</li>
            </ol>
        </div>
    </div><!-- /.row -->
    <form action="<?php echo base_url('index.php/import/save'); ?>" method="post" enctype="multipart/form-data">
        Select image to upload:
        <input type="file" name="import" id="fileToUpload">
        <input type="submit" value="Upload Image" name="submit">
    </form>