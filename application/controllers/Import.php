<?php

class Import extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Import_model', 'import');
        $this->load->library('excel');
        $this->load->library('upload');
        $this->load->model('SiswaModel');

    }

    // upload xlsx|xls file
    public function index()
    {
        $data['page'] = 'import';
        $data['title'] = 'Import XLSX | TechArise';
        $this->load->view('import/index', $data);

    }
    // import excel data
    public function save()
    {
        $file = explode('.', $_FILES['import']['name']);
        $length = count($file);
        if ($file[$length - 1] == 'xlsx' || $file[$length - 1] == 'xls') { //jagain barangkali uploadnya selain file excel <img draggable="false" class="emoji" alt="🙂" src="https://s0.wp.com/wp-content/mu-plugins/wpcom-smileys/twemoji/2/svg/1f642.svg">
            $tmp = $_FILES['import']['tmp_name']; //Baca dari tmp folder jadi file ga perlu jadi sampah di server :-p
            $this->load->library('excel'); //Load library excelnya
            $read = PHPExcel_IOFactory::createReaderForFile($tmp);
            $read->setReadDataOnly(true);
            $excel = $read->load($tmp);
            $sheets = $read->listWorksheetNames($tmp); //baca semua sheet yang ada
            foreach ($sheets as $sheet) {
                if ($this->db->table_exists($sheet)) { //check sheet-nya itu nama table ape bukan, kalo bukan buang aja... nyampah doank :-p
                    $_sheet = $excel->setActiveSheetIndexByName($sheet); //Kunci sheetnye biar kagak lepas :-p
                    $maxRow = $_sheet->getHighestRow();
                    $maxCol = $_sheet->getHighestColumn();
                    $field = array();
                    $sql = array();
                    $maxCol = range('A', $maxCol);
                    foreach ($maxCol as $key => $coloumn) {
                        $field[$key] = $_sheet->getCell($coloumn . '1')->getCalculatedValue(); //Kolom pertama sebagai field list pada table
                    }
                    for ($i = 2; $i <= $maxRow; $i++) {
                        foreach ($maxCol as $k => $coloumn) {
                            $sql[$field[$k]] = $_sheet->getCell($coloumn . $i)->getCalculatedValue();
                        }
                        // $this->db->insert($sheet, $sql); //ribet banget tinggal insert doank...
                        $this->SiswaModel->insert($sheet, $sql);
                    }
                }
            }
        } else {
            exit('do not allowed to upload'); //pesan error tipe file tidak tepat
        }
        // $this->load->view('import/display',);
        echo "berhasil";
    }
}
