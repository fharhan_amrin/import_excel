<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Siswa extends CI_Controller
{
    private $filename = "dataG"; // Kita tentukan nama filenya

    public function __construct()
    {
        parent::__construct();

        $this->load->model('SiswaModel');
    }

    public function index()
    {
        $data['siswa'] = $this->SiswaModel->view();
        $this->load->view('view', $data);
    }

    public function form()
    {
        $data = array(); // Buat variabel $data sebagai array

        if (isset($_POST['preview'])) { // Jika user menekan tombol Preview pada form
            // lakukan upload file dengan memanggil function upload yang ada di SiswaModel.php
            $upload = $this->SiswaModel->upload_file($this->filename);

            if ($upload['result'] == "success") { // Jika proses upload sukses
                // Load plugin PHPExcel nya
                include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

                $excelreader = new PHPExcel_Reader_Excel2007();
                $loadexcel = $excelreader->load('excel/' . $this->filename . '.xlsx'); // Load file yang tadi diupload ke folder excel
                $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);
                // $sheet = $loadexcel->getCell()->toArray(null, true, true, true);
                // Masukan variabel $sheet ke dalam array data yang nantinya akan di kirim ke file form.php
                // Variabel $sheet tersebut berisi data-data yang sudah diinput di dalam excel yang sudha di upload sebelumnya
                $data['sheet'] = $sheet;
            } else { // Jika proses upload gagal
                $data['upload_error'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
            }
        }

        $this->load->view('form', $data);
    }

    public function import()
    {
        // Load plugin PHPExcel nya
        include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

        $excelreader = new PHPExcel_Reader_Excel2007();
        $loadexcel = $excelreader->load('excel/' . $this->filename . '.xlsx'); // Load file yang telah diupload ke folder excel
        $getSheet = $loadexcel->getSheetNames();

        foreach ($getSheet as $rows) {
            $sheet = $loadexcel->getSheetByName($rows)->toArray(null, true, true, true);
            // $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);
            // var_dump($sheet);
            // Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
            $data = array();

            $numrow = 1;
            foreach ($sheet as $row) {
                // Cek $numrow apakah lebih dari 1
                // Artinya karena baris pertama adalah nama-nama kolom
                // Jadi dilewat saja, tidak usah diimport
                if ($numrow > 1) {
                    // Kita push (add) array data ke variabel data
                    array_push($data, array(
                        'serialNumber' => $row['B'], // Insert data nis dari kolom A di excel
                        'tanggalUpload' => date('Y-m-d'), // Insert data nama dari kolom B di excel
                        'code' => $row['A'], // Insert data jenis kelamin dari kolom C di excel
                        'refillId' => $rows, // Insert data alamat dari kolom D di excel
                        'status' => 0, // Insert data alamat dari kolom D di excel
                    ));
                }

                $numrow++; // Tambah 1 setiap kali looping
            }

            // echo json_encode($data);
            // Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
            $this->SiswaModel->insert_multiple($data);
        }

        redirect("Siswa"); // Redirect ke halaman awal (ke controller siswa fungsi index)
    }

    public function inputexcel()
    {

        $id_toko = $this->session->userdata('ap_toko');
        $level = $this->session->userdata('ap_level');
        if ($level != 'admin' or $level == 'inventory') {
            redirect();
        }

        $path = 'assets/excel/';
        if (!file_exists($path)) {
            mkdir($path);
        }

        $path = 'assets/excel/' . $id_toko;
        if (!file_exists($path)) {
            mkdir($path);
        }
        $tmp_name = $_FILES["upload_data"]["tmp_name"];
        $filename = basename($_FILES["upload_data"]["name"]);
        move_uploaded_file($tmp_name, 'assets/excel/' . $id_toko . '/' . $filename);
        $this->load->model('Phpexcel_model');

        $uploadexcel = $this->Phpexcel_model->upload_data($filename, $id_toko);
        unlink('assets/excel/' . $id_toko . '/' . $filename);

        redirect('barang');
    }
}
